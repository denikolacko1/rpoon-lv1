﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV1
{
    class Notes
    {
        private string text;
        private string author;
        private int importance;
        
        public void setImportance(int value) { this.importance = value; }
        public void setText(string text) { this.text = text; }
        public int getImportance() { return this.importance; }
        public string getText() { return this.text; }
        public string getAuthor() { return this.author; }
        
        public Notes()
        {
            this.text = "Pythagorean theorem";
            this.author = "Pythagoras";
            this.importance = 1;
        }
        public Notes(string text,string author,int importance)
        {
            this.text = text;
            this.author = author;
            this.importance = importance;
        }
        public Notes(string text,string author)
        {
            this.text = text;
            this.author = author;
            this.importance = 2;
        }
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
        }
        public int Importance
        {
            get { return this.importance; }
            set { this.importance = value; }
        }
        public override string ToString()
        {
            return (" The name of Author is : " + this.author + " and his text is - " + this.text);
        }

    }
}
