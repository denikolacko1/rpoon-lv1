﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV1
{
    class TimeNotes : Notes
    {
        private DateTime time;
        public TimeNotes() { this.time = DateTime.Now; }
       
        public TimeNotes(string text,string author,int importance,DateTime Time) : base(text,author,importance)
        {
            this.time = Time;
        }
        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }
        public override string ToString() 
        { return base.ToString() + " and the time is : " + this.time ; }
    }
}
